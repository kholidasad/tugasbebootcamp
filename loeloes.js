// fungsi dengan tiga parameter, yaitu : nama, nilai, dan absen
function lulus(nama, nilai, absen){
    let abjad
    let kelulusan
    if (nilai > 100 || nilai < 0) {
        abjad = 'Nilai Invalid'
    } else if(nilai > 79){
        abjad = 'A'
    } else if (nilai > 65) {
        abjad = 'B'
    }
    else if (nilai > 49) {
        abjad = 'C'
    }
    else if (nilai > 34) {
        abjad = 'D'
    }
    else if (nilai >= 0) {
        abjad = 'E'
    } 

    if (absen < 5 && nilai >= 70){
        kelulusan = 'Lulus'
    } else {
        kelulusan = 'Tidak Lulus'
    }

    console.log(nama)
    console.log(abjad)
    if (abjad != 'Nilai Invalid') {
        console.log(kelulusan)
    }
}

//memanggil fungsi lulus dan mengisinya sesuai dengan parameter diatas
lulus("Kocheng", 10, 2)